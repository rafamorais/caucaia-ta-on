<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->after('password', function ($table) {
                $table->string('telefone');
                $table->string('cpf');
                $table->string('cep')->nullable();
                $table->string('endereco');
                $table->smallInteger('numero');
                $table->string('bairro');
                $table->string('complemento')->nullable();
                $table->string('sexo');
                $table->string('uf');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('telefone');
            $table->dropColumn('cpf');
            $table->dropColumn('cep');
            $table->dropColumn('endereco');
            $table->dropColumn('numero');
            $table->dropColumn('bairro');
            $table->dropColumn('complemento');
            $table->dropColumn('sexo');
            $table->dropColumn('uf');
        });
    }
}
