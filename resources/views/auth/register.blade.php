@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('CPF') }}</label>

                            <div class="col-md-6">
                                <input id="cpf" type="text" class="form-control @error('cpf') is-invalid @enderror"
                                    name="cpf" value="{{ old('cpf') }}"  autocomplete="cpf" autofocus>

                                @error('cpf')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}"  autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        @php

                        $sexo = [
                            "MASCULINO",
                            "FEMINIO",
                            "OUTROS",
                            "PREFIRO NÃO DIZER",
                        ];

                    @endphp

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="sexo" class="label-form-custom">{{ __('Sexo') }}</label>
                            <div class="input-container">
                                <select id='sexo' name="sexo" class="form-control @error('sexo') is-invalid @enderror" />
                                <option value="">Selecione o sexo</option>
                                @foreach($sexo as $value)
                                <option value="{{$value}}" {{ $value ==  old('sexo') ? 'selected' : '' }}>{{ $value }}
                                </option>
                                @endforeach
                                </select>
                                @error('sexo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>


                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="telefone" class="label-form-custom">{{ __('Telefone') }}</label>
                                <div class="input-container">
                                    <input id="telefone" type="text"
                                        class="form-control @error('telefone') is-invalid @enderror" name="telefone"
                                        value="{{ old('telefone') }}" placeholder="Digite seu Telefone" />
                                    @error('telefone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="cep" class="label-form-custom">{{ __('CEP') }}</label>
                                <small id="loadCep" style="display: none;"><i class="fa fa-spinner fa-spin"></i>
                                    Carregando...</small>
                                <div class="input-container">
                                    <input id="cep" type="text" class="form-control @error('cep') is-invalid @enderror"
                                        name="cep" value="{{ old('cep') }}" placeholder="Digite seu CEP" />
                                    @error('cep')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="endereco" class="label-form-custom">{{ __('Endereco') }}</label>
                                <div class="input-container">
                                    <input id="endereco" type="text"
                                        class="form-control @error('endereco') is-invalid @enderror" name="endereco"
                                        value="{{ old('endereco') }}" placeholder="Digite seu endereço" />
                                    @error('endereco')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="numero" class="label-form-custom">{{ __('Número') }}</label>
                                <div class="input-container">
                                    <input id="numero" type="number"
                                        class="form-control @error('numero') is-invalid @enderror" name="numero"
                                        value="{{ old('numero') }}" placeholder="Digite seu número" />
                                    @error('numero')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="complemento" class="label-form-custom">{{ __('Complemento') }}</label>
                                <div class="input-container">
                                    <input id="complemento" type="text"
                                        class="form-control @error('complemento') is-invalid @enderror"
                                        name="complemento" value="{{ old('complemento') }}"
                                        placeholder="Digite seu complemento (caso possua)" />
                                    @error('complemento')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="bairro" class="label-form-custom">{{ __('Bairro') }}</label>
                                <div class="input-container">
                                    <input id="bairro" type="text"
                                        class="form-control @error('bairro') is-invalid @enderror" name="bairro"
                                        value="{{ old('bairro') }}" placeholder="Digite seu bairro" />
                                    @error('bairro')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="cidade" class="label-form-custom">{{ __('Cidade') }}</label>
                                <div class="input-container">
                                    <input id="cidade" type="text"
                                        class="form-control @error('cidade') is-invalid @enderror" name="cidade"
                                        value="{{ old('cidade') }}" placeholder="Digite sua cidade" />
                                    @error('cidade')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        @php
                        $estados = [
                        'AC' => 'Acre',
                        'AL' => 'Alagoas',
                        'AP' => 'Amapá',
                        'AM' => 'Amazonas',
                        'BA' => 'Bahia',
                        'CE' => 'Ceará',
                        'DF' => 'Distrito Federal',
                        'ES' => 'Espírito Santo',
                        'GO' => 'Goiás',
                        'MA' => 'Maranhão',
                        'MT' => 'Mato Grosso',
                        'MS' => 'Mato Grosso do Sul',
                        'MG' => 'Minas Gerais',
                        'PA' => 'Pará',
                        'PB' => 'Paraíba',
                        'PR' => 'Paraná',
                        'PE' => 'Pernambuco',
                        'PI' => 'Piauí',
                        'RJ' => 'Rio de Janeiro',
                        'RN' => 'Rio Grande do Norte',
                        'RS' => 'Rio Grande do Sul',
                        'RO' => 'Rondônia',
                        'RR' => 'Roraima',
                        'SC' => 'Santa Catarina',
                        'SP' => 'São Paulo',
                        'SE' => 'Serigipe',
                        'TO' => 'Tocantins'
                        ];
                        @endphp
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="uf" class="label-form-custom">{{ __('Estado') }}</label>
                                <div class="input-container">
                                    <select id='uf' name="uf" class="form-control @error('uf') is-invalid @enderror" />
                                    <option value="">Selecione o estado</option>
                                    @foreach($estados as $key => $value)
                                    <option value="{{$key}}" {{ $key==old('uf') ? 'selected' : '' }}>{{ $value }}
                                    </option>
                                    @endforeach
                                    </select>
                                    @error('uf')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('Senha') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                     autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Senha') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation"  autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
