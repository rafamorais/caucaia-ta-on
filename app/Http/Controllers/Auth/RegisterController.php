<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {   

        $cpf = Helper::apenasNumeros($data['cpf']);
        $telefone = Helper::apenasNumeros($data['telefone']);

        //Retira a mascara para o validador unique funcionar
        $data = array_replace($data, ['cpf' => $cpf]);
        $data = array_replace($data, ['telefone' => $telefone]);
        
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'cpf' => ['required', 'unique:users', 'cpf'],
            'endereco' => ['required', 'min:4' ],
            'bairro' => ['required', 'min:4' ],
            'cidade' => ['required', 'min:4' ],
            'uf' => ['required'],
            'telefone' => ['required', 'min:10'],
            'numero' => ['required', 'max:5'],
            'sexo' => ['required'],
        ];

        $messages = [
            'required' => 'O campo precisa ser preenchido',

            'name.min' => 'O campo nome precisa de no minimo 5 caracteres',
            'name.max' => 'O campo nome suporta no máximo 60 caracteres',
            'email.mail' => 'Digite um e-mail válido',
            'email.max' => 'O campo nome suporta no máximo 80 caracteres',
            'email.unique' => 'Esse E-mail já está sendo utilizado',
            'cpf.unique' => 'O campo :attribute já está sendo utilizado',
            'cpf.cpf' => 'CPF Inválido. Por favor, preencha novamente.',
            
            'password.min' => 'O campo Senha precisa de no minimo 8 caracteres',
            'password.confirmed' => 'O campo confirmar senha precisar combinar com com campo senha',
           // 'cep.formato_cep' => 'Digite um cep válido.',
            'endereco.min' => 'O campo endereço precisa de no minimo 4 caracteres',
            'bairro.min' => 'O campo bairro precisa de no minimo 4 caracteres',
            'cidade.min' => 'O campo cidade precisa de no minimo 4 caracteres',
            'telefone.min' => 'O campo telefone precisa de no minimo 10 caracteres',
            'numero.numeric' => 'Apenas números',
            'numero.max' => 'O campo número suporta no máximo 5 caracteres',
        ];

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {   
        //Colocar mascara no telefone
        $telefone = Helper::apenasNumeros($data['telefone']);
        $telefone = Helper::telefone($telefone);

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'cpf' => Helper::apenasNumeros($data['cpf']),
            'telefone' => $telefone,
            'cep' => $data['cep'],
            'endereco' => $data['endereco'],
            'numero' => $data['numero'],
            'complemento' => $data['complemento'],
            'bairro' => $data['bairro'],
            'cidade' => $data['cidade'],
            'uf' => $data['uf'],
            'sexo' => $data['sexo'],
        ]);

    }
}
